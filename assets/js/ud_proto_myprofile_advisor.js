// JavaScript Document
$(document).ready(function(){
	
	$('#creTarLst').on('click', function() {
		$('#sendtoOMS').addClass('closed');
		var $chlg = $('input:checkbox[name=bobemail_cbox]:checked').length;
		$('#noOfCnt').html($chlg);
		$('#noOfCntPu').html($chlg);		
	});
	
	$('#emptyTarLst').on('click', function() {
		$('#tbl_bob').find('.bobemail_cbox').attr('checked',false);
		$('#noOfCnt').html(0);
		$('#noOfCntPu').html(0);
	});
	
	$('#SAssignCancelOK, #Send2OMS').on('click', function() {
		var $chlg = $('input:checkbox[name=bobemail_cbox]:checked').length;
		$('#noOfCnt').html($chlg);
		$('#noOfCntPu').html($chlg);
		if($chlg > 0){
			$('.modal,.modal-overlay').hide();
			if($(this).attr('id') == 'SAssignCancelOK'){
				document.location.href='create_outreach_version1.html';
			}
		}
	});
	
	var $chcot = $('input:radio[name=creat_outreach_temp_radio]');
	$chcot.on('click change',function(e){
		$('#Outreach_scheduling').find('a').eq(0).addClass('expanded').removeClass('collapsed');
		$('#Outreach_scheduling').next('.content').show();
		var $in = $(this, $(e.currentTarget)).attr('data-instruction');
		var $inst = $in.split('_');
		$("#selectedcust").removeClass('closed');
		var $addTd = $("#selectedcust table tbody tr td");
		$addTd.eq(0).html($inst[0]);$addTd.eq(1).html($inst[1]);$addTd.eq(2).html($inst[2]);
		$addTd.eq(3).html($inst[3]);$addTd.eq(4).html($inst[4]);
	});	
	
	var $wvScY = $('#outreach-schedulingYes')
		$wvScN = $('#outreach-schedulingNo');
	$wvScY.on('click change',function(){
		$('#waveCalculator').find('input:[type=text], select').removeAttr('disabled');
		$('#waveCalculator').find('.btn2').removeClass('btnOff');
	});
	
	$wvScN.on('click change',function(){
		$('#waveCalculator').find('input:[type=text], select').attr('disabled','disabled');
		$('#waveCalculator').find('.btn2').addClass('btnOff');
		$("#outSchedule").addClass("closed");
	});
	
	var $reset = $('#resetVals');
		$reset.on('click', function(){
			$('#waveCalculator').find('input:[type=text], select').attr('disabled','disabled');
			$('#waveCalculator').find('.btn2').addClass('btnOff');
			$("#outSchedule").addClass("closed");
		});
	
	var $calS = $('#calSched');
	$calS.on('click',function(){
		$("#outSchedule").removeClass("closed");
	});
	
	var $conSub = $('#confirmsubmission_outreach');
	$conSub.on('click',function(){
		$('input:[name=outreach_radio]:checked').closest('tr').find('td').eq(3).html("Pending QC");
		$("#submit_OR").find('span').html("Submit to QC");
	});
	
	$("#submit_OR").on('click', function(){
		$("#submit_OR").find('span').html("Submit to Advisor");
	});
});